/**
 * Bpelguardarcontactopom_client_epLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM;

import ffm.confirmacion.util.URIBPEL;

public class Bpelguardarcontactopom_client_epLocator extends org.apache.axis.client.Service implements com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.Bpelguardarcontactopom_client_ep {

    public Bpelguardarcontactopom_client_epLocator() {
    }


    public Bpelguardarcontactopom_client_epLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpelguardarcontactopom_client_epLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BPELGuardarContactoPOM_pt
   // private java.lang.String BPELGuardarContactoPOM_pt_address = "http://10.216.8.198:80/soa-infra/services/SalesForce/OperacionesPOM/bpelguardarcontactopom_client_ep"; //Original
    private java.lang.String BPELGuardarContactoPOM_pt_address = URIBPEL.urlBpelPOM;

    public java.lang.String getBPELGuardarContactoPOM_ptAddress() {
        return BPELGuardarContactoPOM_pt_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BPELGuardarContactoPOM_ptWSDDServiceName = "BPELGuardarContactoPOM_pt";

    public java.lang.String getBPELGuardarContactoPOM_ptWSDDServiceName() {
        return BPELGuardarContactoPOM_ptWSDDServiceName;
    }

    public void setBPELGuardarContactoPOM_ptWSDDServiceName(java.lang.String name) {
        BPELGuardarContactoPOM_ptWSDDServiceName = name;
    }

    public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM getBPELGuardarContactoPOM_pt() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BPELGuardarContactoPOM_pt_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBPELGuardarContactoPOM_pt(endpoint);
    }

    public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM getBPELGuardarContactoPOM_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOMBindingStub _stub = new com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOMBindingStub(portAddress, this);
            _stub.setPortName(getBPELGuardarContactoPOM_ptWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBPELGuardarContactoPOM_ptEndpointAddress(java.lang.String address) {
        BPELGuardarContactoPOM_pt_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM.class.isAssignableFrom(serviceEndpointInterface)) {
                com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOMBindingStub _stub = new com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOMBindingStub(new java.net.URL(BPELGuardarContactoPOM_pt_address), this);
                _stub.setPortName(getBPELGuardarContactoPOM_ptWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BPELGuardarContactoPOM_pt".equals(inputPortName)) {
            return getBPELGuardarContactoPOM_pt();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://SOA.totalplay.com/SalesForce/OperacionesPOM/BPELGuardarContactoPOM", "bpelguardarcontactopom_client_ep");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://SOA.totalplay.com/SalesForce/OperacionesPOM/BPELGuardarContactoPOM", "BPELGuardarContactoPOM_pt"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BPELGuardarContactoPOM_pt".equals(portName)) {
            setBPELGuardarContactoPOM_ptEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

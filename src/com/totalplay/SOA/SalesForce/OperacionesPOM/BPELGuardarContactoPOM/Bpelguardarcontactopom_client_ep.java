/**
 * Bpelguardarcontactopom_client_ep.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM;

public interface Bpelguardarcontactopom_client_ep extends javax.xml.rpc.Service {
    public java.lang.String getBPELGuardarContactoPOM_ptAddress();

    public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM getBPELGuardarContactoPOM_pt() throws javax.xml.rpc.ServiceException;

    public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM getBPELGuardarContactoPOM_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

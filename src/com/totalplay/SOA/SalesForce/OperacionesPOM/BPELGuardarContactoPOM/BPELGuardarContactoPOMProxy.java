package com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM;

public class BPELGuardarContactoPOMProxy implements com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM {
  private String _endpoint = null;
  private com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM bPELGuardarContactoPOM = null;
  
  public BPELGuardarContactoPOMProxy() {
    _initBPELGuardarContactoPOMProxy();
  }
  
  public BPELGuardarContactoPOMProxy(String endpoint) {
    _endpoint = endpoint;
    _initBPELGuardarContactoPOMProxy();
  }
  
  private void _initBPELGuardarContactoPOMProxy() {
    try {
      bPELGuardarContactoPOM = (new com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.Bpelguardarcontactopom_client_epLocator()).getBPELGuardarContactoPOM_pt();
      if (bPELGuardarContactoPOM != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bPELGuardarContactoPOM)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bPELGuardarContactoPOM)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bPELGuardarContactoPOM != null)
      ((javax.xml.rpc.Stub)bPELGuardarContactoPOM)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOM getBPELGuardarContactoPOM() {
    if (bPELGuardarContactoPOM == null)
      _initBPELGuardarContactoPOMProxy();
    return bPELGuardarContactoPOM;
  }
  
  public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessResponseResponse process(com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessLogin login, java.lang.String contactId, java.lang.String contactGroupName, java.lang.String phoneNumber1, java.lang.String phoneNumber2, java.lang.String firstName, java.lang.String lastName, java.lang.String email) throws java.rmi.RemoteException{
    if (bPELGuardarContactoPOM == null)
      _initBPELGuardarContactoPOMProxy();
    return bPELGuardarContactoPOM.process(login, contactId, contactGroupName, phoneNumber1, phoneNumber2, firstName, lastName, email);
  }
  
  
}
/**
 * BPELGuardarContactoPOM.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM;

public interface BPELGuardarContactoPOM extends java.rmi.Remote {
    public com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessResponseResponse process(com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessLogin login, java.lang.String contactId, java.lang.String contactGroupName, java.lang.String phoneNumber1, java.lang.String phoneNumber2, java.lang.String firstName, java.lang.String lastName, java.lang.String email) throws java.rmi.RemoteException;
}

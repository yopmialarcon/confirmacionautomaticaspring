package ffm.confirmacion.entities;

import java.io.Serializable;

public class FFM_AA_PARAMETROS implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1660266509024541084L;

	private int FAP_SEQ;
	
	private int FAP_ID;
	
	private int FAP_STATUS;
	
	private String FAP_VALOR;

	public FFM_AA_PARAMETROS() {
		
	}

	public FFM_AA_PARAMETROS(int fAP_SEQ, int fAP_ID, int fAP_STATUS, String fAP_VALOR) {
		super();
		FAP_SEQ = fAP_SEQ;
		FAP_ID = fAP_ID;
		FAP_STATUS = fAP_STATUS;
		FAP_VALOR = fAP_VALOR;
	}

	public int getFAP_SEQ() {
		return FAP_SEQ;
	}

	public void setFAP_SEQ(int fAP_SEQ) {
		FAP_SEQ = fAP_SEQ;
	}

	public int getFAP_ID() {
		return FAP_ID;
	}

	public void setFAP_ID(int fAP_ID) {
		FAP_ID = fAP_ID;
	}

	public int getFAP_STATUS() {
		return FAP_STATUS;
	}

	public void setFAP_STATUS(int fAP_STATUS) {
		FAP_STATUS = fAP_STATUS;
	}

	public String getFAP_VALOR() {
		return FAP_VALOR;
	}

	public void setFAP_VALOR(String fAP_VALOR) {
		FAP_VALOR = fAP_VALOR;
	}

	@Override
	public String toString() {
		return "FFM_AA_PARAMETROS [FAP_SEQ=" + FAP_SEQ + ", FAP_ID=" + FAP_ID + ", FAP_STATUS=" + FAP_STATUS
				+ ", FAP_VALOR=" + FAP_VALOR + "]";
	}
		
}

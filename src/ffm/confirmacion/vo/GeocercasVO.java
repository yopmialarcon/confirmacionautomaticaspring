package ffm.confirmacion.vo;

public class GeocercasVO {

	private int fct_id;
	
	private float startTime;
	
	private float endTime;

	public GeocercasVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GeocercasVO(int fct_id, float startTime, float endTime) {
		super();
		this.fct_id = fct_id;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public int getFct_id() {
		return fct_id;
	}

	public void setFct_id(int fct_id) {
		this.fct_id = fct_id;
	}

	public float getStartTime() {
		return startTime;
	}

	public void setStartTime(float startTime) {
		this.startTime = startTime;
	}

	public float getEndTime() {
		return endTime;
	}

	public void setEndTime(float endTime) {
		this.endTime = endTime;
	}
	
}

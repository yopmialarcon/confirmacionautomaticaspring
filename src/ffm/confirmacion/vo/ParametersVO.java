package ffm.confirmacion.vo;

import java.util.List;

public class ParametersVO {

	private  int updateOnline;
	
	private int delayAssign;
	
	private float timeStart;
	
	private float timeEnd;
	
	private float now;
	
	private int buclesExe;	
	
	private String qGetFlagReset;
	
	private String qGetUpdateFlag;
	
	private String qTimeZone;
	
	private int sleepTime;
	
	private String qGetGeocercas;
	
	private String qGetOTsCercanas;
	
	private String wsPOM;
	
	private String userPOM;
	
	private String passPOM;
	
	private float maxEsperaOTSinCambios;
	
	private float maxEsperaEscenario2;
	
	private float minEsperaEscenario2;
	
	private float maxEsperaSigueNuevo;
	
	private float minEsperaSigueNuevo;
	
	private int sleepEscenario1y2;
	
	private int sleepEscenario2y3;
	
	private int sleepEscenario3y4;
	
	private int evitaDobleRegistro;
	
	private String statusConfirmadaPOM;
	
	private String campania1;
	
	private String campania2;
	
	private String qInsertAA;
	
	private String qUpdateAA;
	
	private String qGetAA;
	
	private int habilitaEsc2;
	
	private int habilitaEsc3;
	
	private List<GeocercasVO> listGeocercasVO;

	public ParametersVO() {
	}

	public ParametersVO(int updateOnline, int delayAssign, float timeStart, float timeEnd, float now, int buclesExe,
			String qGetFlagReset, String qGetUpdateFlag, String qTimeZone, int sleepTime, String qGetGeocercas,
			String qGetOTsCercanas, String wsPOM, String userPOM, String passPOM, float maxEsperaOTSinCambios,
			float maxEsperaEscenario2, float minEsperaEscenario2, float maxEsperaSigueNuevo, float minEsperaSigueNuevo,
			int sleepEscenario1y2, int sleepEscenario2y3, int sleepEscenario3y4, int evitaDobleRegistro,
			String statusConfirmadaPOM, String campania1, String campania2, String qInsertAA, String qUpdateAA,
			String qGetAA, int habilitaEsc2, int habilitaEsc3, List<GeocercasVO> listGeocercasVO) {
		super();
		this.updateOnline = updateOnline;
		this.delayAssign = delayAssign;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.now = now;
		this.buclesExe = buclesExe;
		this.qGetFlagReset = qGetFlagReset;
		this.qGetUpdateFlag = qGetUpdateFlag;
		this.qTimeZone = qTimeZone;
		this.sleepTime = sleepTime;
		this.qGetGeocercas = qGetGeocercas;
		this.qGetOTsCercanas = qGetOTsCercanas;
		this.wsPOM = wsPOM;
		this.userPOM = userPOM;
		this.passPOM = passPOM;
		this.maxEsperaOTSinCambios = maxEsperaOTSinCambios;
		this.maxEsperaEscenario2 = maxEsperaEscenario2;
		this.minEsperaEscenario2 = minEsperaEscenario2;
		this.maxEsperaSigueNuevo = maxEsperaSigueNuevo;
		this.minEsperaSigueNuevo = minEsperaSigueNuevo;
		this.sleepEscenario1y2 = sleepEscenario1y2;
		this.sleepEscenario2y3 = sleepEscenario2y3;
		this.sleepEscenario3y4 = sleepEscenario3y4;
		this.evitaDobleRegistro = evitaDobleRegistro;
		this.statusConfirmadaPOM = statusConfirmadaPOM;
		this.campania1 = campania1;
		this.campania2 = campania2;
		this.qInsertAA = qInsertAA;
		this.qUpdateAA = qUpdateAA;
		this.qGetAA = qGetAA;
		this.habilitaEsc2 = habilitaEsc2;
		this.habilitaEsc3 = habilitaEsc3;
		this.listGeocercasVO = listGeocercasVO;
	}

	public int getUpdateOnline() {
		return updateOnline;
	}

	public void setUpdateOnline(int updateOnline) {
		this.updateOnline = updateOnline;
	}

	public int getDelayAssign() {
		return delayAssign;
	}

	public void setDelayAssign(int delayAssign) {
		this.delayAssign = delayAssign;
	}

	public float getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(float timeStart) {
		this.timeStart = timeStart;
	}

	public float getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(float timeEnd) {
		this.timeEnd = timeEnd;
	}

	public float getNow() {
		return now;
	}

	public void setNow(float now) {
		this.now = now;
	}

	public int getBuclesExe() {
		return buclesExe;
	}

	public void setBuclesExe(int buclesExe) {
		this.buclesExe = buclesExe;
	}

	public String getqGetFlagReset() {
		return qGetFlagReset;
	}

	public void setqGetFlagReset(String qGetFlagReset) {
		this.qGetFlagReset = qGetFlagReset;
	}

	public String getqGetUpdateFlag() {
		return qGetUpdateFlag;
	}

	public void setqGetUpdateFlag(String qGetUpdateFlag) {
		this.qGetUpdateFlag = qGetUpdateFlag;
	}

	public String getqTimeZone() {
		return qTimeZone;
	}

	public void setqTimeZone(String qTimeZone) {
		this.qTimeZone = qTimeZone;
	}

	public int getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(int sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getqGetGeocercas() {
		return qGetGeocercas;
	}

	public void setqGetGeocercas(String qGetGeocercas) {
		this.qGetGeocercas = qGetGeocercas;
	}

	public String getqGetOTsCercanas() {
		return qGetOTsCercanas;
	}

	public void setqGetOTsCercanas(String qGetOTsCercanas) {
		this.qGetOTsCercanas = qGetOTsCercanas;
	}

	public String getWsPOM() {
		return wsPOM;
	}

	public void setWsPOM(String wsPOM) {
		this.wsPOM = wsPOM;
	}

	public String getUserPOM() {
		return userPOM;
	}

	public void setUserPOM(String userPOM) {
		this.userPOM = userPOM;
	}

	public String getPassPOM() {
		return passPOM;
	}

	public void setPassPOM(String passPOM) {
		this.passPOM = passPOM;
	}

	public float getMaxEsperaOTSinCambios() {
		return maxEsperaOTSinCambios;
	}

	public void setMaxEsperaOTSinCambios(float maxEsperaOTSinCambios) {
		this.maxEsperaOTSinCambios = maxEsperaOTSinCambios;
	}

	public float getMaxEsperaEscenario2() {
		return maxEsperaEscenario2;
	}

	public void setMaxEsperaEscenario2(float maxEsperaEscenario2) {
		this.maxEsperaEscenario2 = maxEsperaEscenario2;
	}

	public float getMinEsperaEscenario2() {
		return minEsperaEscenario2;
	}

	public void setMinEsperaEscenario2(float minEsperaEscenario2) {
		this.minEsperaEscenario2 = minEsperaEscenario2;
	}

	public float getMaxEsperaSigueNuevo() {
		return maxEsperaSigueNuevo;
	}

	public void setMaxEsperaSigueNuevo(float maxEsperaSigueNuevo) {
		this.maxEsperaSigueNuevo = maxEsperaSigueNuevo;
	}

	public float getMinEsperaSigueNuevo() {
		return minEsperaSigueNuevo;
	}

	public void setMinEsperaSigueNuevo(float minEsperaSigueNuevo) {
		this.minEsperaSigueNuevo = minEsperaSigueNuevo;
	}

	public int getSleepEscenario1y2() {
		return sleepEscenario1y2;
	}

	public void setSleepEscenario1y2(int sleepEscenario1y2) {
		this.sleepEscenario1y2 = sleepEscenario1y2;
	}

	public int getSleepEscenario2y3() {
		return sleepEscenario2y3;
	}

	public void setSleepEscenario2y3(int sleepEscenario2y3) {
		this.sleepEscenario2y3 = sleepEscenario2y3;
	}

	public int getSleepEscenario3y4() {
		return sleepEscenario3y4;
	}

	public void setSleepEscenario3y4(int sleepEscenario3y4) {
		this.sleepEscenario3y4 = sleepEscenario3y4;
	}

	public int getEvitaDobleRegistro() {
		return evitaDobleRegistro;
	}

	public void setEvitaDobleRegistro(int evitaDobleRegistro) {
		this.evitaDobleRegistro = evitaDobleRegistro;
	}

	public String getStatusConfirmadaPOM() {
		return statusConfirmadaPOM;
	}

	public void setStatusConfirmadaPOM(String statusConfirmadaPOM) {
		this.statusConfirmadaPOM = statusConfirmadaPOM;
	}

	public String getCampania1() {
		return campania1;
	}

	public void setCampania1(String campania1) {
		this.campania1 = campania1;
	}

	public String getCampania2() {
		return campania2;
	}

	public void setCampania2(String campania2) {
		this.campania2 = campania2;
	}

	public String getqInsertAA() {
		return qInsertAA;
	}

	public void setqInsertAA(String qInsertAA) {
		this.qInsertAA = qInsertAA;
	}

	public String getqUpdateAA() {
		return qUpdateAA;
	}

	public void setqUpdateAA(String qUpdateAA) {
		this.qUpdateAA = qUpdateAA;
	}

	public String getqGetAA() {
		return qGetAA;
	}

	public void setqGetAA(String qGetAA) {
		this.qGetAA = qGetAA;
	}

	public int getHabilitaEsc2() {
		return habilitaEsc2;
	}

	public void setHabilitaEsc2(int habilitaEsc2) {
		this.habilitaEsc2 = habilitaEsc2;
	}

	public int getHabilitaEsc3() {
		return habilitaEsc3;
	}

	public void setHabilitaEsc3(int habilitaEsc3) {
		this.habilitaEsc3 = habilitaEsc3;
	}

	public List<GeocercasVO> getListGeocercasVO() {
		return listGeocercasVO;
	}

	public void setListGeocercasVO(List<GeocercasVO> listGeocercasVO) {
		this.listGeocercasVO = listGeocercasVO;
	}
}

package ffm.confirmacion.vo;

import java.time.LocalDateTime;

public class AgendamientoAutomaticoVO {
	private int fotID;
	private String noCuenta;
	private String nombre;
	private String telFijo;
	private String telMovil;
	private String ordenServicio;
	private String fechaAgendamiento;
	private String turno;
	private String idsf;
	private String tipoLlamada;
	private int idAgendamientoAutomaticoIVR;
	private String codigoDigito;
	private String statusLlamada;
	private int idTecnico;
	private String fotDateEnd;
	private String resultIdAgendamientoAutomaticoIVR;
	private String resultDescriptionAgendamientoAutomaticoIVR;
	private String fechaCreacion;
	
	public AgendamientoAutomaticoVO() {
	}

	public AgendamientoAutomaticoVO(int fotID, String noCuenta, String nombre, String telFijo, String telMovil,
			String ordenServicio, String fechaAgendamiento, String turno, String idsf, String tipoLlamada,
			int idAgendamientoAutomaticoIVR, String codigoDigito, String statusLlamada, int idTecnico,
			String fotDateEnd, String resultIdAgendamientoAutomaticoIVR,
			String resultDescriptionAgendamientoAutomaticoIVR, String fechaCreacion) {
		super();
		this.fotID = fotID;
		this.noCuenta = noCuenta;
		this.nombre = nombre;
		this.telFijo = telFijo;
		this.telMovil = telMovil;
		this.ordenServicio = ordenServicio;
		this.fechaAgendamiento = fechaAgendamiento;
		this.turno = turno;
		this.idsf = idsf;
		this.tipoLlamada = tipoLlamada;
		this.idAgendamientoAutomaticoIVR = idAgendamientoAutomaticoIVR;
		this.codigoDigito = codigoDigito;
		this.statusLlamada = statusLlamada;
		this.idTecnico = idTecnico;
		this.fotDateEnd = fotDateEnd;
		this.resultIdAgendamientoAutomaticoIVR = resultIdAgendamientoAutomaticoIVR;
		this.resultDescriptionAgendamientoAutomaticoIVR = resultDescriptionAgendamientoAutomaticoIVR;
		this.fechaCreacion = fechaCreacion;
	}

	public int getFotID() {
		return fotID;
	}

	public void setFotID(int fotID) {
		this.fotID = fotID;
	}

	public String getNoCuenta() {
		return noCuenta;
	}

	public void setNoCuenta(String noCuenta) {
		this.noCuenta = noCuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelFijo() {
		return telFijo;
	}

	public void setTelFijo(String telFijo) {
		this.telFijo = telFijo;
	}

	public String getTelMovil() {
		return telMovil;
	}

	public void setTelMovil(String telMovil) {
		this.telMovil = telMovil;
	}

	public String getOrdenServicio() {
		return ordenServicio;
	}

	public void setOrdenServicio(String ordenServicio) {
		this.ordenServicio = ordenServicio;
	}

	public String getFechaAgendamiento() {
		return fechaAgendamiento;
	}

	public void setFechaAgendamiento(String fechaAgendamiento) {
		this.fechaAgendamiento = fechaAgendamiento;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getIdsf() {
		return idsf;
	}

	public void setIdsf(String idsf) {
		this.idsf = idsf;
	}

	public String getTipoLlamada() {
		return tipoLlamada;
	}

	public void setTipoLlamada(String tipoLlamada) {
		this.tipoLlamada = tipoLlamada;
	}

	public int getIdAgendamientoAutomaticoIVR() {
		return idAgendamientoAutomaticoIVR;
	}

	public void setIdAgendamientoAutomaticoIVR(int idAgendamientoAutomaticoIVR) {
		this.idAgendamientoAutomaticoIVR = idAgendamientoAutomaticoIVR;
	}

	public String getCodigoDigito() {
		return codigoDigito;
	}

	public void setCodigoDigito(String codigoDigito) {
		this.codigoDigito = codigoDigito;
	}

	public String getStatusLlamada() {
		return statusLlamada;
	}

	public void setStatusLlamada(String statusLlamada) {
		this.statusLlamada = statusLlamada;
	}

	public int getIdTecnico() {
		return idTecnico;
	}

	public void setIdTecnico(int idTecnico) {
		this.idTecnico = idTecnico;
	}

	public String getFotDateEnd() {
		return fotDateEnd;
	}

	public void setFotDateEnd(String fotDateEnd) {
		this.fotDateEnd = fotDateEnd;
	}

	public String getResultIdAgendamientoAutomaticoIVR() {
		return resultIdAgendamientoAutomaticoIVR;
	}

	public void setResultIdAgendamientoAutomaticoIVR(String resultIdAgendamientoAutomaticoIVR) {
		this.resultIdAgendamientoAutomaticoIVR = resultIdAgendamientoAutomaticoIVR;
	}

	public String getResultDescriptionAgendamientoAutomaticoIVR() {
		return resultDescriptionAgendamientoAutomaticoIVR;
	}

	public void setResultDescriptionAgendamientoAutomaticoIVR(String resultDescriptionAgendamientoAutomaticoIVR) {
		this.resultDescriptionAgendamientoAutomaticoIVR = resultDescriptionAgendamientoAutomaticoIVR;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public String toString() {
		return "AgendamientoAutomaticoVO [fotID=" + fotID + ", noCuenta=" + noCuenta + ", nombre=" + nombre
				+ ", telFijo=" + telFijo + ", telMovil=" + telMovil + ", ordenServicio=" + ordenServicio
				+ ", fechaAgendamiento=" + fechaAgendamiento + ", turno=" + turno + ", idsf=" + idsf + ", tipoLlamada="
				+ tipoLlamada + ", idAgendamientoAutomaticoIVR=" + idAgendamientoAutomaticoIVR + ", codigoDigito="
				+ codigoDigito + ", statusLlamada=" + statusLlamada + ", idTecnico=" + idTecnico + ", fotDateEnd="
				+ fotDateEnd + ", resultIdAgendamientoAutomaticoIVR=" + resultIdAgendamientoAutomaticoIVR
				+ ", resultDescriptionAgendamientoAutomaticoIVR=" + resultDescriptionAgendamientoAutomaticoIVR
				+ ", fechaCreacion=" + fechaCreacion + "]";
	}
	
	
}

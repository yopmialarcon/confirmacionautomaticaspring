package ffm.confirmacion.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ffm.confirmacion.bi.ConfirmacionAutomaticaBI;
import ffm.confirmacion.util.Constantes;
import ffm.confirmacion.util.IMessages;
import ffm.confirmacion.vo.ParametersVO;



public class MainConfirmacionAutomatica {
	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");
	private static ConfirmacionAutomaticaBI confirmacionBI = (ConfirmacionAutomaticaBI) applicationContext.getBean("confirmacionBI");
	private static IMessages messages = (IMessages) applicationContext.getBean("messages");
	private static ParametersVO parametersVO = new ParametersVO();
	
	public static void main(String[] args) {
		System.out.println(Constantes.UR);
		System.out.println(Constantes.US);
		//System.out.println(Constantes.PS);
		getArguments(args);
		getParameters();
		
		while(process() == 1) {
			getParameters();
		}

	}

	private static void getArguments(String[] args) {
		try {
			confirmacionBI.getArguments(args);
		}catch(Exception e) {
			messages.console("Codigo error: 001", "Error: Algo salio mal al ejecutar getArguments exception: " + e.getMessage());
		}
		
	}
	
	private static void getParameters() {
		try {
			messages.console("main", "Inicia obtener parametros");
			parametersVO = confirmacionBI.getParameters();	
		} catch (Exception e) {
			messages.console("Codigo error: 002", "Algo salio mal al ejecutar getParameters exception: " + e.getMessage());
		}
		
	}
	
	private static int process() {

		int resetParameters = 0;
		try {
			messages.console("main", "Iniciando");
			resetParameters = confirmacionBI.process(parametersVO);
		}catch(Exception e) {
			messages.console("Codigo error: 003", "Algo salio mal al ejecutar process: " + e.getMessage());
		}
		return resetParameters;
	
	}
}

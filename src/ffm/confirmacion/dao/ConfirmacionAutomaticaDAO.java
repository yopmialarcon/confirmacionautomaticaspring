package ffm.confirmacion.dao;

import java.util.List;

import ffm.confirmacion.entities.FFM_AA_PARAMETROS;
import ffm.confirmacion.vo.AgendamientoAutomaticoVO;
import ffm.confirmacion.vo.GeocercasVO;

public interface ConfirmacionAutomaticaDAO {

	public List<FFM_AA_PARAMETROS> findByFapId();

	public void updateFlagResetParam(String getqGetUpdateFlag);

	public int getByFapId(String getqGetFlagReset, int fap_id);

	public int getTimeZone(String getqTimeZone, int fct_id);

	public List<AgendamientoAutomaticoVO> getOTByConfirm(GeocercasVO operador, String getqGetOTsCercanas, int turn);

	public int saveAgendamientoAutomatico(String query, AgendamientoAutomaticoVO oTEscenario1);

	public void updateAA(String query, String status, int idAA);

	public void callGuardaContactoPom(AgendamientoAutomaticoVO oTEscenario1, String userPOM, String passPOM, String campania);

	public void findAgendamientoAutomatico(String getqGetAA, AgendamientoAutomaticoVO aa);

}

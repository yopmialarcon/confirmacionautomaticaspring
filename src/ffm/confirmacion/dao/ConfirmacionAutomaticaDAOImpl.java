package ffm.confirmacion.dao;

import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.BPELGuardarContactoPOMProxy;
import com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessResponseResponse;

import ffm.confirmacion.entities.FFM_AA_PARAMETROS;
import ffm.confirmacion.util.Constantes;
import ffm.confirmacion.util.IMessages;
import ffm.confirmacion.vo.AgendamientoAutomaticoVO;
import ffm.confirmacion.vo.GeocercasVO;

@Component
public class ConfirmacionAutomaticaDAOImpl implements ConfirmacionAutomaticaDAO{

	
	private JdbcTemplate jdbcTemplateFFM;
	private JdbcTemplate jdbcTemplateKVM;
	
	@Autowired
	private IMessages messages;

	@Autowired
	private void setDataSource(javax.sql.DataSource dataSourceA, javax.sql.DataSource dataSourceB) {
		this.jdbcTemplateFFM = new JdbcTemplate(dataSourceA);
		this.jdbcTemplateKVM = new JdbcTemplate(dataSourceB);

	}

	@Override
	public List<FFM_AA_PARAMETROS> findByFapId() {
		try {			
			Object[] p = { Constantes.CONFIG, Constantes.CONFIG + 999 };
			
			
			BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(FFM_AA_PARAMETROS.class);
			
			return (List<FFM_AA_PARAMETROS>) jdbcTemplateFFM.query("SELECT * FROM FFM_AA_PARAMETROS WHERE FAP_ID BETWEEN ? AND ?", p, bprm);
			
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("findByFapId", "Algo salio mal al buscar parametros: " + o.toString());		
			}
			return null;
		}
	}
	
	@Override
	public int getByFapId(String qGetFlagReset, int fap_id) {
		try {
			Object[] p = { fap_id };
			return jdbcTemplateFFM.queryForObject(qGetFlagReset, p, new RowMapper<Integer>() {

				@Override
				public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getInt(2);
				}
			});
		} catch (Exception e) {
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				return 0;
			}		
			for (Object o : e.getStackTrace()) {
				messages.console("getByFapId", "Algo salio mal al obtener parametros " + o.toString());		
			}
		}
		return 0;
	}
	
	@Override
	public int getTimeZone(String qTimeZone, int fct_id) {
		try {
			Object[] p = { fct_id };
			return jdbcTemplateFFM.queryForObject(qTimeZone, p, new RowMapper<Integer>() {

				@Override
				public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getInt(3);
				}
			});
		} catch (Exception e) {
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				return 0;
			}
			for (Object o : e.getStackTrace()) {
				messages.console("getTimeZone", "Algo salio mal al obtener Asignacion inicial: " + o.toString());		
			}
		}
		return 0;
	}
	
	@Override
	public List<AgendamientoAutomaticoVO> getOTByConfirm(GeocercasVO operador, String qGetOTsCercanas, int turn) {
		try {
			Object[] p = { operador.getFct_id(), turn };
			return jdbcTemplateFFM.query(qGetOTsCercanas, p, new RowMapper<AgendamientoAutomaticoVO>() {

				@Override
				public AgendamientoAutomaticoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					AgendamientoAutomaticoVO agendamientoAutomaticoVO = new AgendamientoAutomaticoVO();
					agendamientoAutomaticoVO.setFotID(rs.getInt(1));
					agendamientoAutomaticoVO.setIdTecnico(rs.getInt(2));
					agendamientoAutomaticoVO.setNoCuenta(rs.getString(3));
					agendamientoAutomaticoVO.setNombre(rs.getString(4));
					agendamientoAutomaticoVO.setTelFijo(rs.getString(5));
					agendamientoAutomaticoVO.setTelMovil(rs.getString(6));
					agendamientoAutomaticoVO.setOrdenServicio(rs.getString(7));
					agendamientoAutomaticoVO.setFechaAgendamiento(rs.getString(8));
					agendamientoAutomaticoVO.setTurno(rs.getString(9));
					agendamientoAutomaticoVO.setIdsf(rs.getString(10));	
					agendamientoAutomaticoVO.setTipoLlamada(rs.getInt(11) == 1 ? "CONFIRMAR" : "PRECONFIRMAR");
					return agendamientoAutomaticoVO;
				}
			});
		} catch (Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("getOTByConfirm", "Algo salio mal al obtener Asignacion inicial: " + o.toString());		
			}
			return null;
		}
	}
	
	@Override
	public int saveAgendamientoAutomatico(String query, AgendamientoAutomaticoVO aa) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {

			jdbcTemplateKVM.update(connection -> {
		        PreparedStatement ps = connection
		          .prepareStatement(query,new String[]{"AA_ID"});
		          ps.setString(1, aa.getNoCuenta());
		          ps.setString(2, aa.getNombre());
		          ps.setLong(3, Long.parseLong(aa.getTelFijo()));
		          ps.setLong(4, Long.parseLong(aa.getTelMovil()));
		          ps.setString(5, "email");
		          ps.setString(6, "urlaudio");
		          ps.setString(7, aa.getOrdenServicio());
		          ps.setString(8, String.valueOf(aa.getFotID()));
		          ps.setString(9, aa.getFechaAgendamiento());
		          ps.setString(10, aa.getTurno());
		          ps.setString(11, "00:00");
		          ps.setString(12, "Nuevo");
		          ps.setString(13, aa.getIdsf());
		          ps.setString(14, "");
		          ps.setString(15, aa.getTipoLlamada());
		          ps.setString(16, "");
		          ps.setString(17, String.valueOf(aa.getIdTecnico()));
		          ps.setString(18, "POM");
		          return ps;
		        }, keyHolder);
			Number key = keyHolder.getKey();
			messages.console("saveAgendamientoAutomatico", "Se guardo con exito OT: "+aa.getFotID() +" ID AA: "+key.intValue());
			return key.intValue();
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("saveAgendamientoAutomatico", "Algo salio mal: "+ o.toString());		
			}
		}
		
		return 0;
	}	
	
	@Override
	public void updateAA(String query, String status, int idAA) {
		try {	
			Object[] p = { status, idAA };
			jdbcTemplateKVM.update(query,p);		
			messages.console("updateAA", "ID AA: "+ idAA +" Actualizado Correctamente");
		}catch(Exception e) {	
			for (Object o : e.getStackTrace()) {
				messages.console("updateAA", "Algo salio mal: "+ o.toString());		
			}
		}
		
	}
	
	@Override
	public void callGuardaContactoPom(AgendamientoAutomaticoVO agenVO, String usr, String pass, String campania) {
		String routine = "callGuardaContactoPom";
		try {
			BPELGuardarContactoPOMProxy pomProxy = new BPELGuardarContactoPOMProxy();
			ProcessResponseResponse result = new ProcessResponseResponse();
			com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessLogin  l = new com.totalplay.SOA.SalesForce.OperacionesPOM.BPELGuardarContactoPOM.ProcessLogin ();
			l.setIp("1.1.1.1");
			l.setUser(usr);
			l.setPassword(pass);
			
			try {
				result = pomProxy.process(l, String.valueOf(agenVO.getIdAgendamientoAutomaticoIVR()), campania, agenVO.getTelMovil(), agenVO.getTelFijo(), agenVO.getNombre()+"|"+agenVO.getIdAgendamientoAutomaticoIVR(), String.valueOf(agenVO.getFotID()), "");			
			} catch (RemoteException e) {
				messages.console(routine, "Algo salio mal, exception: "+ e.getMessage());
			}
			if(result != null) {
				messages.console(routine, "Resultado ID: "+ result.getIdResult() +" Resultado: "+ result.getResult() +" Descripcion: "+result.getResultDescription() );
			}
			
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console(routine, "Algo salio mal: "+ o.toString());		
			}
		}
	}
	
	@Override
	public void findAgendamientoAutomatico(String qGetAA, AgendamientoAutomaticoVO aa) {
		try {
			Object[] p = { aa.getIdAgendamientoAutomaticoIVR() };
			jdbcTemplateKVM.queryForObject(qGetAA, p, new RowMapper<AgendamientoAutomaticoVO>() {

				@Override
				public AgendamientoAutomaticoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					aa.setStatusLlamada(rs.getString(13));
					String[] h = rs.getString(20).split(" ");
					aa.setFechaCreacion(h[1]);
					return aa;
				}
			});
		} catch (Exception e) {	
			for (Object o : e.getStackTrace()) {
				messages.console("findAgendamientoAutomatico", "Algo salio mal al buscar id: "+ o.toString());		
			}
		}
		
	}
	
	@Override
	public void updateFlagResetParam(String qGetUpdateFlag) {
		try {	
			
			jdbcTemplateFFM.update(qGetUpdateFlag,Constantes.CONFIG+Constantes.FAP_ID_UPDATE_ONLINE);		
	
		}catch(Exception e) {	
			for (Object o : e.getStackTrace()) {
				messages.console("updateFlagResetParam", "Algo salio mal al actualizar parametro: "+ o.toString());		
			}
		}
		
	}	
}

package ffm.confirmacion.bi;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ffm.confirmacion.dao.ConfirmacionAutomaticaDAO;
import ffm.confirmacion.entities.FFM_AA_PARAMETROS;
import ffm.confirmacion.util.Constantes;
import ffm.confirmacion.util.IMessages;
import ffm.confirmacion.util.URIBPEL;
import ffm.confirmacion.vo.AgendamientoAutomaticoVO;
import ffm.confirmacion.vo.GeocercasVO;
import ffm.confirmacion.vo.ParametersVO;


@Component("confirmacionBI")
public class ConfirmacionAutomaticaBImpl implements ConfirmacionAutomaticaBI{
	
	@Autowired
	IMessages messages;
	
	@Autowired
	ConfirmacionAutomaticaDAO confirmacionDAO;

	@Override
	public void getArguments(String[] args) {
		try {
			if (args != null) {
				Constantes.CONFIG = Integer.parseInt(args[0]);
			} else {
				messages.console("Argumentos", "Algo salio mal, los argumentos vienen vacios");
			}
		} catch (Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("Argumentos", "Algo salio mal al obtener argumentos, exception: " + o.toString());		
			}
		}
	}

	@Override
	public ParametersVO getParameters() {

		try {
			List<FFM_AA_PARAMETROS> listFFM_AA_PARAMETROS = confirmacionDAO.findByFapId();
			List<GeocercasVO> listGeocercasVO = new ArrayList<GeocercasVO>();
			ParametersVO parametersVO = new ParametersVO();
			if(listFFM_AA_PARAMETROS != null) {
				for (FFM_AA_PARAMETROS fap : listFFM_AA_PARAMETROS) {
					System.out.println(fap);
					
					
					switch(fap.getFAP_ID() - Constantes.CONFIG) {
					
					case Constantes.FAP_ID_UPDATE_ONLINE :
						parametersVO.setUpdateOnline(fap.getFAP_STATUS());
						break;
					case Constantes.FAP_ID_PERIOD :
						String[] period = fap.getFAP_VALOR().split(",");
						if(period[1] != null) {
							parametersVO.setTimeStart(parseHour(period[1]));
						}else {
							messages.console("getParameters", "No se pudo obtener la hora inicio de ejecucion");
						}
						if(period[2] != null) {
							parametersVO.setTimeEnd(parseHour(period[2]));
						}else {
							messages.console("getParameters", "No se pudo obtener la hora fin de ejecucion");
						}
						parametersVO.setNow(parseHour("" + LocalTime.now()));
						break;
					case Constantes.Q_TIMEZONE :
						parametersVO.setqTimeZone(fap.getFAP_VALOR());
						break;
					case Constantes.FAP_ID_SLEEPTIME :
						parametersVO.setSleepTime(fap.getFAP_STATUS());						
						break;
					case Constantes.Q_GETOTSCERCANAS :
						parametersVO.setqGetOTsCercanas(fap.getFAP_VALOR());				
						break;
					case Constantes.FAP_ID_WSPOM:
						URIBPEL.urlBpelPOM = fap.getFAP_VALOR();
						break;
					case Constantes.FAP_ID_USERPOM :
						String[] user =  fap.getFAP_VALOR().split(":");
						parametersVO.setUserPOM(user[1]);
						break;
					case Constantes.FAP_ID_PASSPOM :
						String[] pass = fap.getFAP_VALOR().split(":");
						parametersVO.setPassPOM(pass[1]);
						break;
					case Constantes.FAP_ID_MAXESPERAOTSINCAMBIOS :
						String[] v = fap.getFAP_VALOR().split(":");
						parametersVO.setMaxEsperaOTSinCambios(Float.parseFloat(v[1]));
						break;
					case Constantes.FAP_ID_MAXESPERAESCENARIO2 :
						String[] v1 = fap.getFAP_VALOR().split(":");
						parametersVO.setMaxEsperaEscenario2(Float.parseFloat(v1[1]));
						break;
					case Constantes.FAP_ID_MINESPERAESCENARIO2 :
						String[] v2 = fap.getFAP_VALOR().split(":");
						parametersVO.setMinEsperaEscenario2(Float.parseFloat(v2[1]));
						break;
					case Constantes.FAP_ID_MAXESPERASIGUENUEVO :
						String[] v3 = fap.getFAP_VALOR().split(":");
						parametersVO.setMaxEsperaSigueNuevo(Float.parseFloat(v3[1]));
						break;
					case Constantes.FAP_ID_MINESPERASIGUENUEVO :
						String[] v4 = fap.getFAP_VALOR().split(":");
						parametersVO.setMinEsperaSigueNuevo(Float.parseFloat(v4[1]));
						break;
					case Constantes.FAP_ID_SLEEPESCENARIO1Y2 :
						parametersVO.setSleepEscenario1y2(fap.getFAP_STATUS());
						break;
					case Constantes.FAP_ID_SLEEPESCENARIO2Y3 :
						parametersVO.setSleepEscenario2y3(fap.getFAP_STATUS());
						break;
					case Constantes.FAP_ID_EVITADOBLEREGISTRO :
						parametersVO.setEvitaDobleRegistro(fap.getFAP_STATUS());
						break;
					case Constantes.FAP_ID_SLEEPESCENARIO3Y4 :
						parametersVO.setSleepEscenario3y4(fap.getFAP_STATUS());
						break;
					case Constantes.STATUSCONFIRMADAPOM :
						String[] v5 = fap.getFAP_VALOR().split(":");
						parametersVO.setStatusConfirmadaPOM(v5[1]);
						break;
					case Constantes.FAP_ID_CAMPANIA1 :
						String[] v6 = fap.getFAP_VALOR().split(":");
						parametersVO.setCampania1(v6[1]);
						break;
					case Constantes.FAP_ID_CAMPANIA2 :
						String[] v7 = fap.getFAP_VALOR().split(":");
						parametersVO.setCampania2(v7[1]);
						break;
					case Constantes.FAP_ID_BUCLES_EXE :
						parametersVO.setBuclesExe(fap.getFAP_STATUS());
						break;					
					case Constantes.FAP_ID_QUERY_GETFLAGRESET :
						parametersVO.setqGetFlagReset(fap.getFAP_VALOR());
						break;
					case Constantes.FAP_ID_QUERY_GETUPDATEFLAGRESET :
						parametersVO.setqGetUpdateFlag(fap.getFAP_VALOR());
						break;
					case Constantes.Q_INSERTAA :
						parametersVO.setqInsertAA(fap.getFAP_VALOR());
						break;
					case Constantes.Q_UPDATEAA :
						parametersVO.setqUpdateAA(fap.getFAP_VALOR());
						break;
					case Constantes.Q_GETAA :
						parametersVO.setqGetAA(fap.getFAP_VALOR());
						break;
					case Constantes.FAP_ID_HABILITA_ESC2 :
						parametersVO.setHabilitaEsc2(fap.getFAP_STATUS());
						break;
					case Constantes.FAP_ID_HABILITA_ESC3 :
						parametersVO.setHabilitaEsc3(fap.getFAP_STATUS());
						break;
					default:
						//Aqui geocercas
						if(fap.getFAP_ID() >= (Constantes.CONFIG+100)) {
							GeocercasVO geocercasVO = new GeocercasVO();
							String[] atributos = fap.getFAP_VALOR().split(";");
							String[] geocerca = atributos[0].split(",");
							String[] startTime = atributos[1].split(",");
							String[] endTime = atributos[2].split(",");
							
							geocercasVO.setFct_id(Integer.parseInt(geocerca[1]));
							geocercasVO.setStartTime(parseHour(startTime[1]) );
							geocercasVO.setEndTime(parseHour(endTime[1]) );
							listGeocercasVO.add(geocercasVO);
						}
					}
				}
				parametersVO.setListGeocercasVO(listGeocercasVO);
			}else {
				messages.console("getParameters", "La lista de parametros se encuentra en null");
			}			
			return parametersVO;
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("getParameters", "Algo salio mal exception: " + o.toString());		
			}
			return null;
		}	
	
	}
	
	@Override
	public int process(ParametersVO parametersVO) {
		try {
			List<AgendamientoAutomaticoVO> listEvitaDobleRegistroContestaDigito2 = new ArrayList<AgendamientoAutomaticoVO>();
			List<AgendamientoAutomaticoVO> finIntentosNoContactados = new ArrayList<AgendamientoAutomaticoVO>();
			int resetParameters = 0;
			int ciclo = 0;
			
			//Ciclico
			
			while(parametersVO.getNow() > parametersVO.getTimeStart()
					&& parametersVO.getNow() < parametersVO.getTimeEnd() && resetParameters == 0
					&&  (ciclo < parametersVO.getBuclesExe() || parametersVO.getBuclesExe() == -1)) {			
				
				
				///AQUI CODIFICAR EL PROCESO////
				if(parametersVO.getListGeocercasVO() != null) {
					//Recorremos geocercas
					for (GeocercasVO operador : parametersVO.getListGeocercasVO()) {
						//Obtenemos hora de diferencia de geocerca y validamos si aun le toca ejecutarse
						
						int diferenciaHorario = confirmacionDAO.getTimeZone(parametersVO.getqTimeZone(),operador.getFct_id());
						
						String localTime = "" + LocalTime.now().plusHours(diferenciaHorario);
						if (parseHour(localTime) > operador.getStartTime() && parseHour(localTime) < operador.getEndTime()) {
							
							List<AgendamientoAutomaticoVO> listEscenario1 = new ArrayList<AgendamientoAutomaticoVO>();
							List<AgendamientoAutomaticoVO> listEscenario2 = new ArrayList<AgendamientoAutomaticoVO>();
							List<AgendamientoAutomaticoVO> listSinContacto = new ArrayList<AgendamientoAutomaticoVO>();							
							List<AgendamientoAutomaticoVO> finalList = new ArrayList<AgendamientoAutomaticoVO>();
							
							//Obtenemos el turno							
							int  turn = getTurn(localTime);
							
							//Obtenemos OTs a confirmar
							List<AgendamientoAutomaticoVO>	listAgendamientoAutomaticoVO = confirmacionDAO.getOTByConfirm(operador,parametersVO.getqGetOTsCercanas(),turn);
							
							for (AgendamientoAutomaticoVO agen : listAgendamientoAutomaticoVO) {
								if(!finalList.isEmpty()) 
								{
									boolean flag = true;
									for (AgendamientoAutomaticoVO f : finalList) 									
										if(agen.getFotID() == f.getFotID() || agen.getIdTecnico() == f.getIdTecnico()  ) 
										{
											flag = false;
											break;
										}
												
									if(flag)
										finalList.add(agen);
								}
								else {
									finalList.add(agen);
								}
							}
							
							for (AgendamientoAutomaticoVO agendamientoAutomaticoVO : finalList) {
								System.out.println(agendamientoAutomaticoVO.getFotID() + " , "+ agendamientoAutomaticoVO.getIdTecnico() +"\n");
							}
							
							if(!finalList.isEmpty()) {
															
								for(int i = 0; i< finalList.size(); i++){
									boolean bandera = true;
									for (AgendamientoAutomaticoVO fin : finIntentosNoContactados) {
										if(fin.getFotID() == finalList.get(i).getFotID()) {
											bandera = false;	
											break;
										}
									}
									//Eliminar despues del cambio de Beto
									if(parametersVO.getEvitaDobleRegistro() == 1) {
										for(int j = 0; j< listEvitaDobleRegistroContestaDigito2.size(); j++)  {
											if(finalList.get(i).getFotID() == listEvitaDobleRegistroContestaDigito2.get(j).getFotID()) {
												bandera = false;	
												break;
											}
											
										}
										
									}
										
									if(bandera) {
										listEscenario1.add(finalList.get(i));	
									}
								}
										
							}else {
								messages.console("Lista vacia", "No hay OTs nuevas por confirmar en la geocerca: "+ operador.getFct_id());							
							}
							
							//Eliminar despues del cambio de Beto
							if(parametersVO.getEvitaDobleRegistro() == 1) {
								if(!listEscenario1.isEmpty()) {
									// Remove duplicates 
							        List<AgendamientoAutomaticoVO>   rListEscenario1 = removeDuplicates(listEscenario1); 
							        listEscenario1 = rListEscenario1;
								}
							}
							
							//Eliminar repetidos de Escenario2							
							if(!listEscenario2.isEmpty()) {
								// Remove duplicates 
						        List<AgendamientoAutomaticoVO>   rListEscenario2 = removeDuplicates(listEscenario2); 
						        listEscenario2 = rListEscenario2;
							}
							//PRIMER ESCENARIO
							//Inserta en Tabla Agendamiento_Automatico para que RT4 tome el registro y realice llamada al cliente.
							for (AgendamientoAutomaticoVO OTEscenario1 : listEscenario1) {
								try {
									int idAA =	confirmacionDAO.saveAgendamientoAutomatico(parametersVO.getqInsertAA(),OTEscenario1);//Inserta en AGENDAMIENTO_AUTOMATICO y obtiene ID consecutivo
									if(idAA != 0) {
										OTEscenario1.setIdAgendamientoAutomaticoIVR(idAA);
										String flujo = "OT_"+ OTEscenario1.getFotID()+ "_CONF_AUTOMATICA_1";
										messages.console("Nuevo", flujo);
										//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
										confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AUTOMATICA_1",idAA);
										//Llamado al WS POM para realizar la llamada

										//confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania1());

									//	confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania1());

									}else {
										messages.console("Escenario 1", "Algo salio mal al insertar en Agendamiento Automatico, No se obtuvo ID");
									}
								}catch(Exception e) {
									for (Object o : e.getStackTrace()) {
										messages.console("Escenario 1", "Algo salio mal exception: " + o.toString());		
									}
								}							
							}
							
							
							if(parametersVO.getHabilitaEsc2() == 1) {
								if(!listEscenario1.isEmpty()) {
									//Sleep time entre insercion a AA y consulta FindAA
									messages.console("Minutos de espera: "+((parametersVO.getSleepEscenario1y2()/1000) / 60), " Entre escenario 1 y escenario 2");
									Thread.sleep(parametersVO.getSleepEscenario1y2());
								}
								
								System.out.println("");
								System.out.println("");
								
								//SEGUNDO ESCENARIO
								//Se consultan respuesta de OTS
								for (AgendamientoAutomaticoVO OTEscenario1 : listEscenario1) {
									try {
										confirmacionDAO.findAgendamientoAutomatico(parametersVO.getqGetAA(),OTEscenario1);//Consulta por ID en AGENDAMIENTO_AUTOMATICO	
										if(!OTEscenario1.getStatusLlamada().equals("")) {
											int idAA = OTEscenario1.getIdAgendamientoAutomaticoIVR();
											float diferencia = parseHour("" + LocalTime.now()) - parseHour(OTEscenario1.getFechaCreacion());//Fecha creacion Hora en que se inserto registo
											if(OTEscenario1.getStatusLlamada().equals("No Contesta")) {										
												if(diferencia > parametersVO.getMinEsperaEscenario2() ) {//0.03 == 3 minutos
													listEscenario2.add(OTEscenario1);											
													String flujo = "OT_"+ OTEscenario1.getFotID()+ " ID: "+idAA+ "_CONF_AGENTE_2";
													messages.console("No Contesta", flujo);
													//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_2",idAA);
													//Llamado al WS POM para realizar la llamada											

													//confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());//POM escenario 2
												}
											}else if(OTEscenario1.getStatusLlamada().equals("CONF_AUTOMATICA_1")) {
												if(diferencia > parametersVO.getMinEsperaEscenario2() ) {//0.03 == 3 minutos
													//listOTsNuevos.add(OTEscenario1);
													listEscenario2.add(OTEscenario1);
													String flujo = "OT_"+ OTEscenario1.getFotID()+ " ID: "+idAA+  "_CONF_AGENTE_2";
													messages.console("S", flujo);
													//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_2",idAA);
													//Llamado al WS POM para realizar la llamada	
													//confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());//POM escenario 2

													confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());//POM escenario 2
												}
											}else if(OTEscenario1.getStatusLlamada().equals("CONF_AUTOMATICA_1")) {
												if(diferencia > parametersVO.getMinEsperaEscenario2() ) {//0.03 == 3 minutos
													//listOTsNuevos.add(OTEscenario1);
													listEscenario2.add(OTEscenario1);
													String flujo = "OT_"+ OTEscenario1.getFotID()+ " ID: "+idAA+  "_CONF_AGENTE_2";
													messages.console("S", flujo);
													//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_2",idAA);
													//Llamado al WS POM para realizar la llamada	
												//	confirmacionDAO.callGuardaContactoPom(OTEscenario1, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());//POM escenario 2

												}	
												//Eliminar despues del cambio de Beto
											} else if(parametersVO.getEvitaDobleRegistro() == 1) {
												if(OTEscenario1.getStatusLlamada().equals("Contesta") || OTEscenario1.getStatusLlamada().equals(parametersVO.getStatusConfirmadaPOM())) {										
													listEvitaDobleRegistroContestaDigito2.add(OTEscenario1);
												}
											}
										}else {
											messages.console("Escenario2", "No se pudo obtener el status OT: "+OTEscenario1.getFotID());
										}
									}catch(Exception e) {
										for (Object o : e.getStackTrace()) {
											messages.console("Escenario 2", "Algo salio mal exception: " + o.toString());		
										}
									}								
								}
							}else {
								messages.console("Escenario2", "La bandera del escenario 2 se encuentra apagada");
								for (AgendamientoAutomaticoVO OTEscenario1 : listEscenario1) {
									confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_2",OTEscenario1.getIdAgendamientoAutomaticoIVR());
								}
								listEscenario2 = listEscenario1;
							}
							
							if(parametersVO.getHabilitaEsc3() == 1) {
								//TERCER ESCENARIO
								if(!listEscenario2.isEmpty()) {
									//Sleep time entre escenario 2 y escenario 3
									messages.console("Minutos de espera: "+((parametersVO.getSleepEscenario2y3()/1000) / 60), " Entre escenario 2 y escenario 3");
									Thread.sleep(parametersVO.getSleepEscenario2y3());
								}
								
								System.out.println("");
								System.out.println("");
								
								for (AgendamientoAutomaticoVO OTEscenario2 : listEscenario2) {
									try {
										confirmacionDAO.findAgendamientoAutomatico(parametersVO.getqGetAA(),OTEscenario2);
										if(!OTEscenario2.getStatusLlamada().equals("")) {
											int idAA = OTEscenario2.getIdAgendamientoAutomaticoIVR();
											float diferencia = parseHour("" + LocalTime.now()) - parseHour(OTEscenario2.getFechaCreacion());
											if(OTEscenario2.getStatusLlamada().equals("No Contesta")) {
												
												if(diferencia >= parametersVO.getMaxEsperaOTSinCambios()) {//Mayor a 1 hora se manda a lista de no procesadas
													listSinContacto.add(OTEscenario2);
													String flujo = "OT_"+ OTEscenario2.getFotID()+  " ID: "+idAA+ "_CONF_AGENTE_3";
													messages.console("No contesta", flujo);
													//Llamado al metodo para actualizar el status en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_3",idAA);
													//Llamado al WS POM para realizar la llamada	

													//confirmacionDAO.callGuardaContactoPom(OTEscenario2, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());
												}
											} else if(OTEscenario2.getStatusLlamada().equals("CONF_AGENTE_2")) {
												if(diferencia >= parametersVO.getMaxEsperaOTSinCambios()) {//Mayor a 1 hora se manda a lista de no procesadas
													listSinContacto.add(OTEscenario2);
													String flujo = "OT_"+ OTEscenario2.getFotID()+  " ID: "+idAA+ "_CONF_AGENTE_3";
													messages.console("Sin contactar", flujo);
													//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_3",idAA);
													//Llamado al WS POM para realizar la llamada	
													//confirmacionDAO.callGuardaContactoPom(OTEscenario2, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());

													confirmacionDAO.callGuardaContactoPom(OTEscenario2, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());
												}
											} else if(OTEscenario2.getStatusLlamada().equals("CONF_AGENTE_2")) {
												if(diferencia >= parametersVO.getMaxEsperaOTSinCambios()) {//Mayor a 1 hora se manda a lista de no procesadas
													listSinContacto.add(OTEscenario2);
													String flujo = "OT_"+ OTEscenario2.getFotID()+  " ID: "+idAA+ "_CONF_AGENTE_3";
													messages.console("Sin contactar", flujo);
													//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
													confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_3",idAA);
													//Llamado al WS POM para realizar la llamada	
												//	confirmacionDAO.callGuardaContactoPom(OTEscenario2, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());

												}
												//Eliminar despues del cambio de Beto
											} else if(parametersVO.getEvitaDobleRegistro() == 1) {
												if(OTEscenario2.getStatusLlamada().equals("Contesta") || OTEscenario2.getStatusLlamada().equals(parametersVO.getStatusConfirmadaPOM())) {										
													listEvitaDobleRegistroContestaDigito2.add(OTEscenario2);
												}
											}
										}else {
											messages.console("Escenario3", "No se pudo obtener el status OT: "+OTEscenario2.getFotID());
										}	
									}catch(Exception e) {
										for (Object o : e.getStackTrace()) {
											messages.console("Escenario 3", "Algo salio mal exception: " + o.toString());		
										}
									}														
								}
							}else {
								messages.console("Escenario3", "La bandera del escenario 3 se encuentra apagada");
								for (AgendamientoAutomaticoVO OTEscenario2 : listEscenario2) {
									confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"CONF_AGENTE_3",OTEscenario2.getIdAgendamientoAutomaticoIVR());
								}
								listSinContacto = listEscenario2;
							}							
							
							//CUARTO ESCENARIO
							if(!listSinContacto.isEmpty()) {
								//Sleep time entre escenario 3 y escenario 4
								messages.console("Minutos de espera: "+((parametersVO.getSleepEscenario3y4()/1000) / 60), " Entre escenario 3 y escenario 4");
								Thread.sleep(parametersVO.getSleepEscenario3y4());
							}
							
							System.out.println("");
							System.out.println("");
							
							for (AgendamientoAutomaticoVO oTSinContacto : listSinContacto) {
								try {
									confirmacionDAO.findAgendamientoAutomatico(parametersVO.getqGetAA(),oTSinContacto);
									int idAA = oTSinContacto.getIdAgendamientoAutomaticoIVR();
									if(!oTSinContacto.getStatusLlamada().equals("")) {
										if(oTSinContacto.getStatusLlamada().equals("No Contesta")) {
											finIntentosNoContactados.add(oTSinContacto);
											messages.console("Sin contactar", "OT_"+ oTSinContacto.getFotID()+  " ID: "+idAA+ " NO_CONTACTADO");
											//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
											confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"NO_CONTACTADO",idAA);
											//Llamado al WS POM para realizar la llamada	

											//confirmacionDAO.callGuardaContactoPom(oTSinContacto, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());
										}else if(oTSinContacto.getStatusLlamada().equals("CONF_AGENTE_3")) {
											finIntentosNoContactados.add(oTSinContacto);
											messages.console("Sin contactar", "OT_"+ oTSinContacto.getFotID()+  " ID: "+idAA+ "NO_CONTACTADO");
											//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
											confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"NO_CONTACTADO",idAA);
											//Llamado al WS POM para realizar la llamada	
											//confirmacionDAO.callGuardaContactoPom(oTSinContacto, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());

											confirmacionDAO.callGuardaContactoPom(oTSinContacto, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());
										}else if(oTSinContacto.getStatusLlamada().equals("CONF_AGENTE_3")) {
											finIntentosNoContactados.add(oTSinContacto);
											messages.console("Sin contactar", "OT_"+ oTSinContacto.getFotID()+  " ID: "+idAA+ "NO_CONTACTADO");
											//Llamado al WS ModifyOT, actualiza estatus en Agendamiento Automatico
											confirmacionDAO.updateAA(parametersVO.getqUpdateAA(),"NO_CONTACTADO",idAA);
											//Llamado al WS POM para realizar la llamada	
										//	confirmacionDAO.callGuardaContactoPom(oTSinContacto, parametersVO.getUserPOM(), parametersVO.getPassPOM(),parametersVO.getCampania2());
											//yopmialarcon@bitbucket.org/yopmialarcon/confirmacionautomaticaspring.git
											//Eliminar despues del cambio de Beto
										}else if(parametersVO.getEvitaDobleRegistro() == 1) {
											if(oTSinContacto.getStatusLlamada().equals("Contesta") || oTSinContacto.getStatusLlamada().equals(parametersVO.getStatusConfirmadaPOM())) {										
												listEvitaDobleRegistroContestaDigito2.add(oTSinContacto);
											}
										}
									}else {
										messages.console("Escenario 4", "No se pudo obtener el status OT: "+oTSinContacto.getFotID());
									}
								}catch(Exception e) {
									for (Object o : e.getStackTrace()) {
										messages.console("Escenario 4", "Algo salio mal exception: " + o.toString());		
									}
								}																
							}
							
						}else {
							messages.console("Mensaje por timezone", "La geocerca: " + operador.getFct_id() + " se encuentra fuera de horario.");
						}
						
					}
				}else {
					messages.console("Mensaje del proceso", "La lista de geocercas viene vacia");
				}			
				////////////////////////////////
				
				//Actualizacion de condiciones
				resetParameters = confirmacionDAO.getByFapId(parametersVO.getqGetFlagReset(),Constantes.CONFIG+Constantes.FAP_ID_UPDATE_ONLINE);
				messages.console("Proceso principal", "Sleep "+ ((parametersVO.getSleepTime()/1000) / 60) +" minutos");
				Thread.sleep(parametersVO.getSleepTime());
				if(resetParameters == 1)
		    	 {
					confirmacionDAO.updateFlagResetParam(parametersVO.getqGetUpdateFlag());
		    	 }
		    	 parametersVO.setNow(parseHour("" + LocalTime.now()));
			     ciclo++;		
			     if(ciclo == parametersVO.getBuclesExe()) {
			    	 System.out.println("Fin de ciclos");
			     }
			}			
			
			return resetParameters;
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				messages.console("process", "Algo salio mal exception: " + o.toString());		
			}
			return 0;
		}
		
		
	}
	
	private float parseHour(String date) {
		String[] parse = date.split(":");
		String format = parse[0]+"."+parse[1];
		return Float.parseFloat(format);
	}	
	
	private int getTurn(String localTime) {
		if(parseHour(localTime) >= 8 && parseHour(localTime) <= 13.3) {
			return 1;
		} else if(parseHour(localTime) >= 13.31 && parseHour(localTime) <= 23) {
			return 2;
		}
		return 1;
	}
	
	 public static <T> List<T> removeDuplicates(List<T> list) 
	    { 
	  
	        // Create a new LinkedHashSet 
	        Set<T> set = new LinkedHashSet<>(); 
	  
	        // Add the elements to set 
	        set.addAll(list); 
	  
	        // Clear the list 
	        list.clear(); 
	  
	        // add the elements of set 
	        // with no duplicates to the list 
	        list.addAll(set); 
	  
	        // return the list 
	        return list; 
	    } 
}

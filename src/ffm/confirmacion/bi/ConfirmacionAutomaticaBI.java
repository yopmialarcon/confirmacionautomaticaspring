package ffm.confirmacion.bi;

import ffm.confirmacion.vo.ParametersVO;

public interface ConfirmacionAutomaticaBI {

	public void getArguments(String[] args);

	public ParametersVO getParameters();

	public int process(ParametersVO parametersVO);

}

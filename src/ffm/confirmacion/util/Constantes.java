package ffm.confirmacion.util;

public class Constantes {
	public static final String MODULE = "FFM CONFIRMACION AUTOMATICA";
	public static final String COMMENT_QUEUE = "FFM CONFIRMACION";
	public static final String SYSTEM = "FFM";
	public static final String VERSION = "2.1";
	
	
	public static final int FAP_ID_PERIOD                         	= 0;  // Horario de ejecucion
	public static final int Q_TIMEZONE								= 1;  // Query TimeZone
	public static final int FAP_ID_SLEEPTIME						= 2;  // SleepTime entre ciclos
	public static final int FAP_ID_UPDATE_ONLINE           			= 3;  // Actualizacion en linea: 1 SI actualizar en linea, 0 NO actualizar en linea');
	public static final int Q_GETGEOCERCAS							= 4;  
	public static final int Q_GETOTSCERCANAS						= 5;  //Obtienes OTs cercanas a una cuadrilla
	public static final int FAP_ID_QUERY_GETUPDATEFLAGRESET        	= 7;  // Query para actualizar reset de parametros despues de obtenerlos
	public static final int	FAP_ID_WSPOM							= 8;
	public static final int FAP_ID_USERPOM							= 9;
	public static final int FAP_ID_PASSPOM							= 10;
	public static final int FAP_ID_MAXESPERAOTSINCAMBIOS			= 16;
	public static final int FAP_ID_MAXESPERAESCENARIO2				= 17;
	public static final int FAP_ID_MINESPERAESCENARIO2				= 18;
	public static final int FAP_ID_MAXESPERASIGUENUEVO	     		= 19;
	public static final int FAP_ID_MINESPERASIGUENUEVO  			= 20;
	public static final int FAP_ID_SLEEPESCENARIO1Y2				= 21;
	public static final int FAP_ID_SLEEPESCENARIO2Y3				= 25;
	public static final int FAP_ID_EVITADOBLEREGISTRO				= 26;
	public static final int FAP_ID_SLEEPESCENARIO3Y4				= 27;	
	public static final int STATUSCONFIRMADAPOM						= 28;
	public static final int FAP_ID_BUCLES_EXE                    	= 29; //Cantidad de ciclos a operar.  -1 infinito o la cantidad de ciclos deseados');
	public static final int FAP_ID_QUERY_GETFLAGRESET              	= 30;  // Query para obtener la bandera de actualizacion de parametros  
	public static final int FAP_ID_CAMPANIA1             			= 31;
	public static final int FAP_ID_CAMPANIA2             			= 32;
	public static final int Q_INSERTAA		             			= 33;
	public static final int Q_GETAA			             			= 34;
	public static final int Q_UPDATEAA		             			= 35;
	public static final int FAP_ID_HABILITA_ESC2           			= 36;
	public static final int FAP_ID_HABILITA_ESC3           			= 37;
	
	

	public static String UR; 
	public static String US; 
	public static String PS; 
	public static String BD;
	public static int CONFIG;
}

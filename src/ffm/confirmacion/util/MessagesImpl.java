package ffm.confirmacion.util;

import java.time.LocalTime;

import org.springframework.stereotype.Component;
@Component("messages")
public class MessagesImpl implements IMessages{

	@Override
	public void console(String rutina, String mensaje) {
		String straux="";
        straux += LocalTime.now()     + ",";
        straux += Constantes.SYSTEM  + ",";
        straux += Constantes.MODULE   + ",";
        straux += Constantes.VERSION  + "," ;
        straux += "Rutina: "+ rutina  + "," ;
        straux += mensaje ;
        System.out.println(straux); 		
	}

}
